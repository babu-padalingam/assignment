##ABC company's proprietary web-portal


##### Create a Python 3.6 pipenv Environment  

Follow instruction    https://docs.pipenv.org/   for installing pipenv.

For activate pipenv Environment by using

```
pipenv shell
```

For deactivate pipenv Environment by using

```
exit
```

##### After activate the pipenv Environment


##### Install dependencies.

```
pip install -r requirements.txt
```


##### To Run the Project

```
python manage.py runserver
```


##### To List the Partners

```
http://localhost:8000/company/partner/
method : GET
```
##### To Create New the Partners

```
http://localhost:8000/company/partner/
method : POST
```
##### To the Particular Partners

```
http://localhost:8000/company/partner/<id>/
method : GET
```
##### To Edit the Particular Partners

```
http://localhost:8000/company/partner/<id>/
method : PUT
```
##### To Delete the Particular Partners

```
http://localhost:8000/company/partner/<id>/
method : DELETE
```


##### To List the Member for Particular Partner

```
http://localhost:8000/company/partner/<partner_id>/member/
method : GET
```
##### To Create New the Partners

```
http://localhost:8000/company/partner/<partner_id>/member/
method : POST
```
##### To the Particular Partners

```
http://localhost:8000/company/partner/<id>/<partner_id>/member/<member_id>/
method : GET
```
##### To Edit the Particular Partners

```
http://localhost:8000/company/partner/<id>/<partner_id>/member/<member_id>/
method : PUT
```
##### To Delete the Particular Partners

```
http://localhost:8000/company/partner/<id>/<partner_id>/member/<member_id>/
method : DELETE
```