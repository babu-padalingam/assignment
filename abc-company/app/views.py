from django.shortcuts import render

from rest_framework import viewsets

from app.models import Partner, Member
from app.serializers import PartnerSerializer, MemberSerializer


class PartnersView(viewsets.ModelViewSet):

    serializer_class = PartnerSerializer
    queryset = Partner.objects.all()


class MembersView(viewsets.ModelViewSet):

    serializer_class = MemberSerializer

    def get_queryset(self):
        return Member.objects.filter(partner_id=self.kwargs['id'])
