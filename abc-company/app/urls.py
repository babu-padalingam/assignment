from django.urls import path
from rest_framework import routers

from app.views import PartnersView, MembersView

router = routers.DefaultRouter()

router.register(prefix=r'^partner', viewset=PartnersView, base_name="app")
router.register(prefix=r'^partner/(?P<id>[0-9]+)/member', viewset=MembersView, base_name="members")

urlpatterns = router.urls
